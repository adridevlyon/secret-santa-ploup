const names = ["Adri", "Blandine", "Diane", "Manon", "Mme Pauline", "Yvan"];

function getNames() {
    return names;
}

const recipients = [1, 3, 4, 5, 0, 2];

function getRecipient(selectedName) {
    const nameIndex = names.findIndex(name => name === selectedName);
    const recipientIndex = recipients[nameIndex];
    return names[recipientIndex];
}
